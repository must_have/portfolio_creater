import csv


class toCsv:
    def __init__(self, path, fieldnames, my_list):
        self.my_list = my_list
        self.fieldnames = fieldnames
        self.path = path

    def write(self):
        """
        path - :)
        fieldnames -field names
        data - list of lists
        """
        with open(self.path, "w", newline='') as out_file:
            '''
            out_file - out data as object
            delimiter - splitter :|;
            fieldnames - field names
            '''
            writer = csv.DictWriter(out_file, delimiter=',', fieldnames=self.fieldnames)
            writer.writeheader()
            for row in self.my_list:
                writer.writerow(row)
        out_file.close()
