# point to start, menu and items to create db, download historical data, datasummary
# s&p500, calculate regression and others
from DB import getDB
from DB import creating_db
from fundametal_data import yahoo_data
from termcolor import colored
from IB import get_historical
from fundametal_data import get_SnP_data
from mathematic import mathematic

if __name__ == '__main__':
    while True:
        print('Is this the first launch? Y/N')
        check = input()
        if check == 'Y' or check == 'y' or check == 'yes':
            DB = getDB.mySqlConnect(None)
            db = creating_db.Create_schema(DB=DB, name='yahoo')
            db.drop_db_ifexists()
            db.create_schema()

            DB = getDB.mySqlConnect('yahoo')
            db = creating_db.Create_schema(DB=DB)

            db.yahoo_company()
            db.yahoo_datasummary()
            db.yahoo_historical()
            db.yahoo_snp()
            print('Get companies from yahoo:')
            getYahoo = yahoo_data.yahoo_data()
            getYahoo.get_companies()
            print('Companies are downloaded')
            break
        elif check == 'N' or check == 'n' or check == 'no':
            break
        else:
            continue
    while True:
        print('1 - download historical data')
        print('2 - download datasummary (dividends,eps,revenue)')
        print('3 - download s&p500 data')
        print('4 - calculate regression')
        print('5 - check if stock strong or not')
        print('6 - exit')
        choice = input()
        if choice == '1':
            print('Wait a few minutes for downloading historical data:')
            try:
                get_historical.data()
            except:
                print(colored('Error while downloading historical data. Please try again.', 'red'))
        elif choice == '2':
            try:
                print('Downloading datasummary')
                get_historical.datasummary()
            except:
                print(colored('Error whille downloading datasummary. Please try again.', 'red'))
        elif choice == '3':
            print('Get s&p500 historical data:')
            get_SnP_data.snp()
        elif choice == '4':
            print('calculate regression, correlation')
            mathematic.calculate_regression()
        elif choice == '5':
            mathematic.strong_or_weak()
        elif choice == '6':
            break
        else:
            print('Error! Try again.')
