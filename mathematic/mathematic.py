"""
covariation
dispersion
cramers rule
regression
search strong and weak stocks
create portfolio

"""

import re
from termcolor import colored
import write_tocsv
from DB import getDB
import numpy as np
from numpy import var
import c_list
import math
from datetime import datetime
import select_data_fromdb
from fundametal_data import parse_nasdaq


def covariation(a, b):
    a_mean = np.mean(a)
    b_mean = np.mean(b)
    len(a)
    len(b)
    sum = 0
    for i in range(0, len(a)):
        try:
            sum += ((a[i] - a_mean) * (b[i] - b_mean))
        except IndexError:
            return 0

    return sum / (len(a) - 1)


def dispersion(etalon):
    disp = var(etalon)
    return disp


def cramers_rule(A, B):
    main_det = np.linalg.det(A)
    A_ = A.copy()
    A_[0][0] = B[0]
    A_[1][0] = B[1]
    second_det = np.linalg.det(A_)
    a = second_det / main_det
    A_ = A.copy()
    A_[0][1] = B[0]
    A_[1][1] = B[1]
    third_det = np.linalg.det(A_)

    b = third_det / main_det
    return b


def regression(x, y):
    print(x)
    print(y)
    print(1)
    all_variables = len(x)
    summa_x = sum(x)
    summa_y = sum(y)
    x_square = [i * i for i in x]
    y_square = [i * i for i in y]
    suma_x_square = sum(x_square)
    summa_y_square = sum(y_square)
    i = 0
    x_on_y = []
    for data in x:
        xy = float(data) * float(y[i])
        x_on_y.append(xy)
        i += 1
    summa_x_on_y = sum(x_on_y)
    A = np.array([[float(all_variables), summa_x], [summa_x, suma_x_square]])
    B = np.array([summa_y, summa_x_on_y])
    b = cramers_rule(A, B)
    not_x = summa_x / all_variables
    not_y = summa_y / all_variables
    Sx = suma_x_square - (not_x ** 2)
    Sy = summa_y_square - (not_y ** 2)
    rxy = b * (math.sqrt(Sx) / math.sqrt(Sy))
    return [rxy, b]


def calculate_regression():
    fundamental = [['Name', 'Sector', 'Beta', 'Correlation', 'Eps Beta', 'Eps Correlation', 'Total Revenue Beta',
                    'Total Revenue Correlation', 'Dividends Beta', 'Dividends Correlation', 'Price x Volume',
                    'Week range high', '52 Week Range', 'Year Target', ' 90 Day Average Volume', 'P/E Ratio']]
    date = str(datetime.now())
    date = re.split(' ', str(date))
    date = re.split('-', date[0])
    year = int(date[0])
    old_date = str(year - 3) + '-' + date[1] + '-' + date[2]
    this_date = re.split(' ', str(datetime.now()))

    DB = getDB.mySqlConnect()
    lists_company = []
    cursor = DB.cursor()
    cursor.execute('select company from historical')
    buffer = ''
    for data in cursor:
        if data[0] == buffer:
            buffer = data[0]
        else:
            lists_company.append(data[0])
            buffer = data[0]
    cursor.close()

    cursor = DB.cursor()
    sector = []
    week_range52 = []
    week_range_high = []
    pricevolume = []
    for data in lists_company:
        cursor.execute(
            'select sector_name, 52_week_range, week_range_high, pricevolume from company where symbol="{}"'.format(
                data))
        for sectors in cursor:
            sector.append(sectors[0])
            week_range52.append(sectors[1])
            week_range_high.append(sectors[2])
            pricevolume.append(sectors[3])
    cursor.close()
    data_summary = {}
    i = 0
    for data in lists_company:
        data_summary.update(select_data_fromdb.getDatasummary(data))
        dividends = data_summary[data]['dividends'][0]
        dividends_date = data_summary[data]['dividends'][1]
        eps = data_summary[data]['EPS'][0]
        total_revenue = data_summary[data]['TotalRevenue'][0]
        dividends.reverse()
        historical_data = select_data_fromdb.getLists(data, this_date[0], old_date)[1]
        snp_data = select_data_fromdb.getLists(data, this_date[0], old_date)[0]  # temp
        historical_data_for_summary = []
        for date in dividends_date:
            historical_data_for_summary.append(select_data_fromdb.getHData(date, data))
        historical_data.reverse()
        eps.reverse()
        snp_data.reverse()
        historical_data_for_summary.reverse()
        try:
            total_revenue.reverse()
            dividends = c_list.List.return_value(dividends)
            historical_data = c_list.List.return_value(historical_data)
            print(historical_data_for_summary)
            historical_data_for_summary = c_list.List.return_value(historical_data_for_summary)
            eps = c_list.List.return_value(eps)
            total_revenue = c_list.List.return_value(total_revenue)
            snp_data = c_list.List.return_value(snp_data)
            print(data, sector[i])

        except:
            print('Error in regression func (line:150)')
            i += 1
            continue
        try:

            nasdaq = parse_nasdaq.GetNasdaq(data)
            addon_data = nasdaq.parse_summary()
            year_target = addon_data[0]
            day_av90 = addon_data[1]
            pe_ratio = addon_data[2]

            fundamental.append([data, sector[i], regression(snp_data, historical_data)[1],
                                regression(snp_data, historical_data)[0],
                                regression(eps, historical_data_for_summary)[1],
                                regression(eps, historical_data_for_summary)[0],
                                regression(total_revenue, historical_data_for_summary)[1],
                                regression(total_revenue, historical_data_for_summary)[0],
                                regression(dividends, historical_data_for_summary)[1],
                                regression(dividends, historical_data_for_summary)[0], pricevolume[i],
                                week_range_high[i], week_range52[i], year_target, day_av90, pe_ratio])
            my_list = []
            fieldnames = fundamental[0]
            cell = fundamental[1:]
            for values in cell:
                inner_dict = dict(zip(fieldnames, values))
                my_list.append(inner_dict)
            path = "dict_output.csv"
            write = write_tocsv.toCsv(path, fieldnames, my_list)
            write.write()

        except EnvironmentError:
            print(colored('Error in regression func (line 180)', 'red'))
            i += 1
            continue
        i += 1


def getListForStrongWeak(portfolio):
    beta = []
    all_uniq_company = list()
    DB = getDB.mySqlConnect()
    for data in portfolio.keys():
        for items in portfolio[data]:
            all_uniq_company.append(items[0])
            beta.append(items[2])
    company_dict_old = dict()
    company_dict_new = dict()
    print('company dicts created')
    cursor = DB.cursor()
    for data in all_uniq_company:
        cursor.execute(
            'select close from historical where company="{}" and date BETWEEN "2018-06-23" AND "2018-06-29"'.format(
                data))
        for _ in cursor:
            close = _[0]
            break
        company_dict_old.update({data: close})
    cursor.close()
    cursor = DB.cursor()
    for data in all_uniq_company:
        cursor.execute(
            'select close from historical where company="{}" order by date desc'.format(data))
        for _ in cursor:
            close = _[0]
            break
        company_dict_new.update({data: close})
    cursor.close()
    print(company_dict_old)  # prices on june 2018
    print(company_dict_new)  # today`s price
    data_old = list()
    data_new = list()
    for data in company_dict_old.keys():
        data_old.append(company_dict_old[data])

    for data in company_dict_new.keys():
        data_new.append(company_dict_new[data])

    sectors_data = list()
    cost_basis = list()
    num_of_shares = list()
    iterator = 0
    for data in portfolio.keys():
        num_cost = 0
        flag = 0
        for _ in portfolio[data]:
            sectors_data.append(data)
            num_cost += float(data_old[iterator])
            iterator += 1
            flag += 1
        if flag == 1:
            cost_basis.append(num_cost)
        else:
            print(num_cost)
            cost_basis.append(num_cost)
            cost_basis.append(num_cost)
    i = 0
    for data in cost_basis:
        s = float(data) / float(data_old[i])
        num_of_shares.append(s)
        i += 1

    # add first column
    data_old = ['Prices on 29 June'] + data_old
    data_new = ['Today`s price'] + data_new
    cost_basis = ['Cost basis'] + cost_basis
    all_uniq_company = ['All companies'] + all_uniq_company
    sectors_data = ['Sector'] + sectors_data
    beta = ['Beta'] + beta
    num_of_shares = ['Number of shares']+num_of_shares
    return [all_uniq_company, cost_basis, data_old, data_new, num_of_shares, beta, sectors_data]


# search stocks that have similar beta and max relation eps/price
def getMaxMinRelations(relations_list=list()):
    if len(relations_list) == 2:
        return relations_list
    else:
        symbol = list()
        relations = list()
        beta = list()
        for data in relations_list:
            symbol.append(data[0])
            relations.append(float(data[1]))
            beta.append(float(data[2]))
        unsorted_relations = relations.copy()

        relations.sort()
        max_relation = relations[-1]
        return_list = list()
        i = 0
        index_ = 0
        for data in unsorted_relations:
            if data == max_relation:
                index_ = i
                return_list.append([symbol[i], data, beta[i]])
                break
            i += 1
        similar_beta = beta[index_]
        for relation_eps in relations:
            i = 0
            for data in unsorted_relations:

                if i == index_:
                    continue
                if data == relation_eps:
                    if -0.2 < (similar_beta - beta[i]) < 0.2:
                        return_list.append([symbol[i], data, beta[i]])
                i += 1
            if len(return_list) == 2:
                break

        return return_list


"""
search strong and weak stocks
1. Select companies and their prices for 2018-06-29 (the last eps from IB)
2. Select from datasummary eps by each company for 2018-06-29
3. Get Beta
4. Calculaate average eps for all companies
5. Select companies that have eps more than average
6. Get pair of stocks that have min eps and max eps and get their historical data for 2018-06-29 and for today`s
7. Calculate cost basis
8. Create portfolio
"""


def strong_or_weak():
    DB = getDB.mySqlConnect()
    # relations_dict = {}
    # all_relations = []
    # cursor = DB.cursor()
    # cursor.execute('select symbol,price,eps from company where price !=0 and eps !=0')
    # company_data = cursor.fetchall()
    # cursor.close()
    # for data in company_data:
    #     eps = float(data[2])
    #     close = float(data[1])
    #     relation = eps / close
    #     all_relations.append(relation)
    #     relations_dict.update({data[0]: relation})
    # average = sum(all_relations)/len(all_relations)
    # i=0
    # flag=0
    # for data in all_relations:
    #
    #     if data>average:
    #         flag+=1
    #     i+=1
    cursor = DB.cursor()
    company_data = list()
    cursor.execute('select company,close from historical where date ="2018-06-29" and close!="0"')
    for data in cursor:
        company_data.append([data[0], data[1]])
    cursor.close()
    cursor = DB.cursor()
    all_relations = list()
    i = 0
    companies = company_data.copy()
    all_index_company = list()
    buffer = 0
    for data in company_data:
        print(data[0])
        cursor.execute(
            'select value from datasummary where tag="EPS" and company="{}" and value != 0 order by date desc'.format(
                data[0]))

        for epss in cursor:
            eps = epss[0]
            break
        try:
            if float(eps) > 55 or buffer == eps:
                all_index_company.append(i)
                del companies[i]
                continue
            all_relations.append(float(eps) / float(data[1]))
        except IndexError:
            del companies[i]
            continue
        buffer = eps
        i += 1
    cursor.close()
    cursor = DB.cursor()
    sector_and_beta = list()
    cursor.execute('select sector_name, beta from company')
    sector_and_beta = cursor.fetchall()  # ((data,data,),...)
    beta_list = []
    for data in sector_and_beta:
        beta_list.append(data[1])
    for indexes in all_index_company:
        del beta_list[indexes]

    average = sum(all_relations) / len(all_relations)
    iterator = int()
    buffer = str()
    data_dict = dict()
    relations = list()
    print('buffer, data_dict and relations are created')
    for data in all_relations:
        sector = sector_and_beta[iterator][0]
        beta = beta_list[iterator]
        if buffer != sector:
            relations = []
            buffer = sector
        if data > average:
            relations.append([companies[iterator][0], data, beta])
            data_dict.update({buffer: relations})
        iterator += 1

    portfolio = dict()
    for data in data_dict.keys():
        buffer_list = []
        for relations in data_dict[data]:
            company_name = relations[0]
            relation = relations[1]
            beta = relations[2]
            buffer_list.append([company_name, relation, beta])
        maxRelations = getMaxMinRelations(buffer_list)
        try:
            portfolio.update({data: maxRelations})
        except IndexError:
            continue
    max_min_relations = getListForStrongWeak(portfolio)
    my_list = []
    fieldnames = max_min_relations[0]
    cell = max_min_relations[1:]
    for values in cell:
        inner_dict = dict(zip(fieldnames, values))
        my_list.append(inner_dict)

    path = "portfolio.csv"
    write = write_tocsv.toCsv(path, fieldnames, my_list)
    write.write()
