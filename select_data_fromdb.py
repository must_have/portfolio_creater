from DB import getDB

DB = getDB.mySqlConnect()


def getDatasummary(data):
    cursor = DB.cursor()
    dividends_ps = []
    eps = []
    total_revenue = []
    date_dps = []
    date_eps = []
    date_tr = []
    cursor.execute('select tag, value, date from datasummary where company="{}" order by date desc'.format(data))
    for summary in cursor:
        if summary[0] == 'DividendPerShare':
            dividends_ps.append(summary[1])
            date_dps.append(summary[2])
        elif summary[0] == 'TotalRevenue':
            total_revenue.append(summary[1])
            date_tr.append(summary[2])
        elif summary[0] == 'EPS':
            eps.append(summary[1])
            date_eps.append(summary[2])
    cursor.close()
    return {data: {'dividends': [dividends_ps, date_dps], 'TotalRevenue': [total_revenue, date_tr],
                   'EPS': [eps, date_eps]}}


def getHData(date, company):
    second_date = date.split('-')
    month = int(second_date[1])
    year = int(second_date[0])
    if month == 12:
        month = '01'
        year += 1
    else:
        month += 1
    second_date = str(year) + '-' + str(month) + '-' + second_date[2]
    cursor = DB.cursor()
    cursor.execute(
        'select close from historical where company="{}" and date BETWEEN "{}" and "{}"'.format(company, date,
                                                                                                second_date))
    a = ''
    for data in cursor:
        a = data[0]
    cursor.close()
    return a


def getSnp(date):
    cursor = DB.cursor()
    second_date = date.split('-')
    month = int(second_date[1])
    year = int(second_date[0])
    if month == 12:
        month = '01'
        year += 1
    else:
        month += 1
    second_date = str(year) + '-' + str(month) + '-' + second_date[2]

    cursor.execute('select close from snp where date BETWEEN "{}" and "{}"'.format(date, second_date))
    a = ''
    for data in cursor:
        a = data[0]
    cursor.close()
    return a


def getLists(name, this_date, old_date):
    snp = []
    company = []
    company_data = []
    cursor = DB.cursor()
    cursor.execute(
        "select company,close from historical where company='{}' and date BETWEEN '{}' and '{}' order by date desc".format(
            name, old_date, this_date))
    i = 0
    for data in cursor:
        if i % 30 == 0:
            company.append(data[0])
            company_data.append(float(data[1]))
        i += 1
    cursor.close()
    cursor = DB.cursor()
    cursor.execute(
        "select close from snp where date BETWEEN '{}' and '{}' order by date desc".format(old_date, this_date))
    i = 0
    for data in cursor:
        if i % 30 == 0:
            snp.append(float(data[0]))
        i += 1
    cursor.close()
    return [snp, company_data]
