# work with price and date lists
import math

correction_up = 2
correction_down = -2


class List:
    def __init__(self, historical_data=list(), snp=list()):
        self.snp = snp
        self.historical_data = historical_data

    @staticmethod
    def make_percent(price_list):
        count = len(price_list)
        i = 0
        percent = 0
        price_percent_list = []
        while i < count:
            percent = ((float(price_list[i]) - float(price_list[0])) / float(price_list[0]) * 100)
            price_percent_list.append(percent)
            i += 1
        return price_percent_list

    @staticmethod
    def search_consolidation_month(price_list):
        # search consolidation on 7 days
        price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
        average_1 = (price_list[0] + price_list[1]) / 2
        average_2 = ((price_list[3] + price_list[4]) / 2)
        average_3 = ((price_list[6] + price_list[7]) / 2)
        # average_4 = ((price_list[9] + price_list[10]) / 2)

        condition1 = average_1 - average_2
        condition2 = average_2 - average_3
        # condition3 = average_3 - average_4
        price_list.reverse()
        if ((correction_up >= condition1 >= correction_down)
            and (correction_up >= condition2 >= correction_down)):
            return True
        else:
            return False

    @staticmethod
    def search_consolidation_month3(price_list):
        # search consolidation on 21 days
        price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
        average_1 = (price_list[0] + price_list[1]) / 2
        average_2 = ((price_list[9] + price_list[12]) / 2)
        average_3 = ((price_list[18] + price_list[21]) / 2)
        # average_4 = ((price_list[9] + price_list[10]) / 2)

        condition1 = average_1 - average_2
        condition2 = average_2 - average_3
        # condition3 = average_3 - average_4
        price_list.reverse()
        if ((correction_up >= condition1 >= correction_down)
            and (correction_up >= condition2 >= correction_down)):
            return True
        else:
            return False

    @staticmethod
    def search_consolidation_month6(price_list):
        # search consolidation on 25 days
        price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
        average_1 = (price_list[0] + price_list[1]) / 2
        average_2 = ((price_list[3] + price_list[4]) / 2)
        average_3 = ((price_list[15] + price_list[16]) / 2)
        average_4 = ((price_list[24] + price_list[25]) / 2)

        condition1 = average_1 - average_2
        condition2 = average_2 - average_3
        condition3 = average_3 - average_4
        price_list.reverse()
        if ((correction_up >= condition1 >= correction_down)
            and (correction_up >= condition2 >= correction_down)
            and (correction_up >= condition3 >= correction_down)):
            return True
        else:
            return False

    @staticmethod
    def search_consolidation_year(price_list):
        # search consolidation on 10 days
        price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
        average_1 = (price_list[0] + price_list[1]) / 2
        average_2 = ((price_list[3] + price_list[4]) / 2)
        average_3 = ((price_list[15] + price_list[16]) / 2)
        average_4 = ((price_list[24] + price_list[25]) / 2)

        condition1 = average_1 - average_2
        condition2 = average_2 - average_3
        condition3 = average_3 - average_4
        price_list.reverse()
        if ((correction_up >= condition1 >= correction_down)
            and (correction_up >= condition2 >= correction_down)
            and (correction_up >= condition3 >= correction_down)):
            return True
        else:
            return False

    @staticmethod
    def search_trend_up(price_list):
        # search consolidation on 5 days
        max = 0
        len_of_list = len(price_list)
        if len_of_list == 21:
            price_list.reverse()
            i = 2
            max = price_list[i]
            while i < len_of_list:
                if i == 6: break
                if max < price_list[i]:
                    max = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо максимальну точку. якшо двома останными перетин то тренд можливий вверх
            if max < main_average and math.fabs(main_average - max) >= 1:
                price_list.reverse()
                return True
            else:
                # 5 days

                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[2] + price_list[3]) / 2)
                average_3 = ((price_list[4] + price_list[5]) / 2)
                price_list.reverse()

                if (average_1 > average_2) and (average_2 > average_3):
                    return True
                else:
                    return False
        elif len_of_list == 63:
            # search consolidation on 14 days
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            max = price_list[i]
            while i < len_of_list:
                if i == 14: break
                if max < price_list[i]:
                    max = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо максимальну точку. якшо двома останными перетин то тренд можливий вверх
            if max < main_average and math.fabs(main_average - max) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                price_list.reverse()
                if (average_1 > average_2) and (average_2 > average_3) and (average_3 > average_4):
                    return True
                else:
                    return False

        elif len_of_list == 126:
            # search consolidation on 22 days
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            max = price_list[i]
            while i < len_of_list:
                if i == 22: break
                if max < price_list[i]:
                    max = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо максимальну точку. якшо двома останными перетин то тренд можливий вверх
            if max < main_average and math.fabs(main_average - max) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                average_5 = ((price_list[16] + price_list[18]) / 2)
                average_6 = ((price_list[20] + price_list[22]) / 2)
                price_list.reverse()
                if (average_1 > average_2) and (average_2 > average_3) and (average_3 > average_4) \
                        and (average_4 > average_5) and (average_5 > average_6):
                    return True
                else:
                    return False

        elif len_of_list == 250:
            # search consolidation on 26 days
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            max = price_list[i]
            while i < len_of_list:
                if i == 26: break
                if max < price_list[i]:
                    max = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо максимальну точку. якшо двома останными перетин то тренд можливий вверх
            if max < main_average and math.fabs(main_average - max) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                average_5 = ((price_list[16] + price_list[18]) / 2)
                average_6 = ((price_list[20] + price_list[22]) / 2)
                average_7 = ((price_list[24] + price_list[26]) / 2)

            price_list.reverse()
            if (average_1 > average_2) and (average_2 > average_3) and (average_3 > average_4) and (
                        average_4 > average_5) and (
                        average_5 > average_6) and (
                        average_6 > average_7):
                return True
            else:
                return False
        else:
            return 'num error'

    @staticmethod
    def search_trend_down(price_list):
        min = 0
        len_of_list = len(price_list)
        if len_of_list == 21:
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            min = price_list[i]
            while i < len_of_list:
                if i == 6: break
                if min > price_list[i]:
                    min = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо мінімальну точку. якшо двома останными перетин то тренд можливий вниз
            if min > main_average and math.fabs(main_average - min) >= 1:
                price_list.reverse()
                return True
            else:
                # 7 days

                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[2] + price_list[3]) / 2)
                average_3 = ((price_list[4] + price_list[5]) / 2)
                price_list.reverse()

                if (average_1 < average_2) and (average_2 < average_3):
                    return True
                else:
                    return False
        elif len(price_list) == 63:
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            min = price_list[i]
            while i < len_of_list:
                if i == 14: break
                if min > price_list[i]:
                    min = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо мінімальну точку. якшо двома останными перетин то тренд можливий вниз
            if min > main_average and math.fabs(main_average - min) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                price_list.reverse()
                if (average_1 < average_2) and (average_2 < average_3) and (average_3 < average_4):
                    return True
                else:
                    return False

        elif len(price_list) == 126:
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            min = price_list[i]
            while i < len_of_list:
                if i == 22: break
                if min > price_list[i]:
                    min = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо мінімальну точку. якшо двома останными перетин то тренд можливий вниз
            if min > main_average and math.fabs(main_average - min) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                average_5 = ((price_list[16] + price_list[18]) / 2)
                average_6 = ((price_list[20] + price_list[22]) / 2)
                price_list.reverse()
                if (average_1 < average_2) and (average_2 < average_3) and (average_3 < average_4) \
                        and (average_4 < average_5) and (average_5 < average_6):
                    return True
                else:
                    return False

        elif len(price_list) == 250:
            price_list.reverse()  # реверс для правильного відображення. (ФІКСИТИ)
            i = 2
            min = price_list[i]
            while i < len_of_list:
                if i == 26: break
                if min > price_list[i]:
                    min = price_list[i]
                    i += 1
                else:
                    i += 1
            main_average = (price_list[0] + price_list[
                1]) / 2  # шукаэмо мінімальну точку. якшо двома останными перетин то тренд можливий вниз
            if min > main_average and math.fabs(main_average - min) >= 1:
                price_list.reverse()
                return True
            else:
                average_1 = (price_list[0] + price_list[1]) / 2
                average_2 = ((price_list[4] + price_list[5]) / 2)
                average_3 = ((price_list[8] + price_list[10]) / 2)
                average_4 = ((price_list[12] + price_list[14]) / 2)
                average_5 = ((price_list[16] + price_list[18]) / 2)
                average_6 = ((price_list[20] + price_list[22]) / 2)
                average_7 = ((price_list[24] + price_list[26]) / 2)

                price_list.reverse()
                if (average_1 < average_2) and (average_2 < average_3) and (average_3 < average_4) and (
                            average_4 < average_5) and (
                            average_5 < average_6) and (
                            average_6 < average_7):
                    return True
                else:
                    return False
        else:
            return 'num error'

    @staticmethod
    def search_consolidation(price_list):
        interval = len(price_list)
        try:
            if interval == 21:
                return List.search_consolidation_month(price_list)
            elif interval == 63:
                return List.search_consolidation_month3(price_list)
            elif interval == 126:
                return List.search_consolidation_month6(price_list)
            elif interval == 250:
                return List.search_consolidation_year(price_list)
        except ValueError as e:
            return e

    @staticmethod
    def return_value(data):
        returns = []
        i = 0
        for _ in data:
            if i == 0:
                returns.append(0)
                i += 1
                continue
            try:
                try:
                    return_value = (float(data[i]) / float(data[i - 1])) - 1

                except ValueError:
                    continue
            except ZeroDivisionError:
                return_value = 0
            returns.append(return_value)
            i += 1
        return returns

    def is_strong(self):
        i = 0
        flag = 0
        historical_percent = self.make_percent(self.historical_data)
        snp_percent = self.make_percent(self.snp)
        for data in historical_percent:
            if data >= snp_percent[i]:
                flag += 1
            i += 1
        print(flag / i)
