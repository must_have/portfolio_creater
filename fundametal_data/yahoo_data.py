import re
import requests
from bs4 import BeautifulSoup
from termcolor import colored
from multiprocessing import Process
from DB import getDB


class yahoo_data:
    def __init__(self, current_volume=0, week_range52=0, price=0, m_cap=0, beta=0, dividents=0):
        self.DB = getDB.mySqlConnect()
        self.dividends = dividents
        self.beta = beta
        self.m_cap = m_cap
        self.price = price
        self.week_range52 = week_range52
        self.current_volume = current_volume
        self.list_stocks = {}
        self.__dict_sector = {'names': ['financial', 'healthcare', 'services', 'utilities',
                                        'industrial_goods', 'basic_materials',
                                        'conglomerates', 'consumer_goods', 'technology']}

    # get_companies calls get_sector_pages (for counting companies on each sector) and then get stock data by
    # each company
    def get_companies(self):
        for data in self.__dict_sector['names']:
            self.get_sector_pages(data)

    def get_sector_pages(self, name_sector):
        link = 'https://finance.yahoo.com/screener/predefined/{}?offset=0&count=100'.format(name_sector)
        r = requests.get(link)
        text = r.text.encode('utf-8')
        soup = BeautifulSoup(text, "lxml")
        count = soup.find('span', {'data-reactid': 20})
        result = re.split(r'of', count.text)
        count = re.split(r' ', result[1])
        count = int(count[1])
        count = int(count) / 100 + 1
        i = 0
        flag = 0
        while i < count:
            self.get_stocks(name_sector, flag)
            flag += 100
            i += 1
            break

    def get_stocks(self, name_sector, flag):
        print(name_sector)
        link = 'https://finance.yahoo.com/screener/predefined/{}?offset={}&count=100'.format(name_sector, str(flag))
        r = requests.get(link)
        text = r.text.encode('utf-8')
        soup = BeautifulSoup(text, "lxml")
        table = soup.find('div', {'id': 'scr-res-table'})
        table_body = table.find('tbody')
        strings = table_body.findAll('tr')
        cursor = self.DB.cursor()
        for data in strings:
            try:
                data_company = data.findAll('td')
                symbol = data_company[0].text
                name = data_company[1].text
                name = re.sub(',', '', name)
                name = re.sub("'", "", name)
                price = data_company[2].text
                price = re.sub(',', '', price)  # заміна коми якшо є
                volume = data_company[5].text
                price_x_volume = float(price) * float(get_market_cup(volume))
                m_cap = data_company[7].text
                try:
                    print(symbol, name)
                    addon_data = parse_company(symbol)  # beta, week range, dividends, link,eps,week range high
                    try:
                        beta = addon_data[0][0]
                        week_range52 = addon_data[0][1]
                        dividends = addon_data[0][2]
                        link = addon_data[1]
                        week_range_high = addon_data[2]
                        eps = addon_data[3]
                    except IndexError:
                        print('Index out of the range')
                        continue
                    print(name)
                    cursor.execute(
                        "insert into company(`name`,`symbol`,`sector_name`,`price`,`volume`,`beta`,`dividents`,`m_cap`,"
                        "`52_week_range`, `week_range_high`,`pricevolume`,`eps`,`link`)"
                        " values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" %
                        (str(name), str(symbol), str(name_sector), str(price), str(volume), str(beta), str(dividends),
                         str(m_cap),
                         str(week_range52), week_range_high, str(price_x_volume), str(eps), str(link)))
                except ValueError:
                    print('Error in parse_company func with this symbol:' + str(symbol))
            except TypeError:
                print('Type Error in get_stocks function')

                # universal function. split data by "_"


def get_market_cup(m_cap):
    # print(m_cap)
    cap = None
    try:
        split = m_cap.split('.')

        if re.search(r'B', split[1]) is not None:
            cap = split[0] + '000000000'
        elif re.search(r'M', split[1]) is not None:
            cap = split[0] + '000000'
        else:
            print('Error in get_market_cup function (get_yahoo):' + m_cap)
    except:
        if re.search(r'B', m_cap) is not None:
            cap = re.sub(r'B', '000000000', m_cap)
        elif re.search(r'M', m_cap) is not None:
            cap = re.sub(r'M', '000000', m_cap)
        else:
            m_cap = re.sub(',', '', m_cap)
            return m_cap

    return cap


def parse_company(symbol):
    link = 'https://finance.yahoo.com/quote/{}?p={}'.format(symbol, symbol)
    r = requests.get(link)
    text = r.text.encode('utf-8')
    soup = BeautifulSoup(text, "lxml")
    beta = soup.find('td', {'data-test': 'BETA_3Y-value'})
    week_range = soup.find('td', {'data-test': 'FIFTY_TWO_WK_RANGE-value'})
    dividends = soup.find('td', {'data-test': 'DIVIDEND_AND_YIELD-value'})
    eps = soup.find('td', {'data-test': 'EPS_RATIO-value'})
    try:
        eps = eps.text
        dividends = dividends.text
        dividends = dividends.split(' ')
        dividends = dividends[1]
        dividends = re.sub('[()%]', '', dividends)

        beta = beta.text
        # scrapped week range " 100 - 200 " for example
        half = week_range.text.split(' - ')
        left_half = half[0].split('.')
        right_half = half[1].split('.')
        left_half = re.sub(',', '', left_half[0])
        right_half = re.sub(',', '', right_half[0])
        week_range = int(right_half) - int(left_half)
        range_and_beta = [[beta, week_range, float(dividends)], link, right_half, eps]
        return range_and_beta
    except AttributeError as e:
        print('Error in parse_company func: ' + str(e))
        return []

